import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  var1 = 'mensaje uno ';
  var2 = 'mensaje dos';
  var3 = 'mensaje tres';
  var4 = 'mensaje cuatro';
  var5 = 'mensaje cinco.'
  
  constructor() { }

  ngOnInit(): void {
  }

}
