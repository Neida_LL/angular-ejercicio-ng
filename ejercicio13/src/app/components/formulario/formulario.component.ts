import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  formulario!:FormGroup;
  valido:boolean=false;
  invalido:boolean=false;
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.formulario = this.fb.group({
      correo:['Escribir Correo Electronico...',[Validators.required,Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
    })
  }
 

  verificar():void{
    if (this.formulario.get('correo')?.valid) {
      this.valido=true;
      this.invalido=false;
      setTimeout(()=>{
      this.valido=false;
      } , 5000);
    } else {
      this.invalido=true;
      this.valido=false;
      setTimeout(()=>{
        this.invalido=false;
        } , 5000);
    }
   }

   borrar():void{
    this.formulario.reset({
      correo:''
    });
   }
}