
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {



  constructor() { }
  
  msje1: string = 'Mensaje 1  ';
  msje2: string = 'mensaje 2 ';
  msje3: string = 'mensaje 3 ';
  msje4: string = 'mensaje 4 ';
  msje5: string = 'mensaje 5 ';

  @Output() msg1 = new EventEmitter<string>();
  @Output() msg2 = new EventEmitter<string>();
  @Output() msg3 = new EventEmitter<string>();
  @Output() msg4 = new EventEmitter<string>();
  @Output() msg5 = new EventEmitter<string>();

  ngOnInit(): void {
    this.msg1.emit(this.msje1);
    this.msg2.emit(this.msje2)
    this.msg3.emit(this.msje3)
    this.msg4.emit(this.msje4)
    this.msg5.emit(this.msje5)
  }

}
