import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  constructor() { }

  mensajePadre! : string;
  SecondMessage! : string;
  thirdMessage!: string;
  fourthMessage!: string;
  fifthMessage!: string;


  obtenerMensaje($event: string): void {
    this.mensajePadre = $event;
    console.log(this.mensajePadre);  
  }

  obtenerSegundo ($event: string): void {
    this.SecondMessage = $event;
  }

  obtenerTercer ($event: string): void {
    this.thirdMessage = $event;
  }

  obtenerCuarto($event: string): void {
    this.fourthMessage = $event;
  }

  obtenerQuinto ($event: string): void {
    this.fifthMessage = $event;
  }

  ngOnInit(): void {
  }

}
