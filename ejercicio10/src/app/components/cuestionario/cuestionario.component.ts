import { Component, OnInit } from '@angular/core';
import { FormBuilder ,FormGroup,FormArray,AbstractControlOptions, Validators} from '@angular/forms';

@Component({
  selector: 'app-cuestionario',
  templateUrl: './cuestionario.component.html',
  styleUrls: ['./cuestionario.component.css']
})
export class CuestionarioComponent implements OnInit {
  submitted = false;
  enviar = false;
  forma!:FormGroup;
  constructor(private fb:FormBuilder) {
    this.crearFormulario();
    // this.cargarFormulario();
   }

  ngOnInit(): void {
  }

  crearFormulario():void{
    this.forma = this.fb.group({
      
      opcion1:['p11',Validators.required],
      opcion2:['p22',Validators.required],
      opcion3:['p33',Validators.required],
      opcion4:['p44',Validators.required],
      opcion5:['p55',Validators.required],
      check:[true,Validators.requiredTrue],

    })
  }

  cargarFormulario():void{
    this.forma.patchValue({
     
       check:true,
    });
  }
  
  get f() { return this.forma.controls;

   }

  guardar(){
    this.submitted = true;
    this.enviar=false;
    // stop here if form is invalid
    if (this.forma.invalid) {
      return;
    }else{
      this.enviar=true;
    }
  }


  onChange():void{
    this.submitted=false;
    this.enviar=false;
  }

}
